<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz</title>
</head>

<body>
    <?php
    $Questions = array(
        1 => array(
            'Question' => '1. CSS stands for',
            'Answers' => array(
                'A' => 'Computer Styled Sections',
                'B' => 'Cascading Style Sheets',
                'C' => 'Crazy Solid Shapes'
            ),
            'CorrectAnswer' => 'B'
        ),
        2 => array(
            'Question' => '2. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        3 => array(
            'Question' => '3. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        4 => array(
            'Question' => '4. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        5 => array(
            'Question' => '5. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        )
    );

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST["answer1"]) && isset($_POST["answer2"]) && isset($_POST["answer3"]) && isset($_POST["answer4"]) && isset($_POST["answer5"])) {
            $answer1 = $_POST["answer1"];
            $answer2 = $_POST["answer2"];
            $answer3 = $_POST["answer3"];
            $answer4 = $_POST["answer4"];
            $answer5 = $_POST["answer5"];
            setcookie("answer1", $answer1, time() + (86400 * 30), "/");
            setcookie("answer2", $answer2, time() + (86400 * 30), "/");
            setcookie("answer3", $answer3, time() + (86400 * 30), "/");
            setcookie("answer4", $answer4, time() + (86400 * 30), "/");
            setcookie("answer5", $answer5, time() + (86400 * 30), "/");
            header("location: page2.php");
        } else {
            echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
        }
    }

    ?>
    <div class="testbox">
        <form method="post" id="quiz">
            <div class="banner">
            </div>
            <?php
                $totalQuestionCount = count($Questions);
                foreach ($Questions as $QuestionNo => $Value) {

                ?>

            <h3><?php echo $Value['Question']; ?></h3>
            <?php
                    foreach ($Value['Answers'] as $Letter => $Answer) {
                        $Label = 'question-' . $QuestionNo . '-answers-' . $Letter;
                    ?>
            <div>
                <input type="radio" name="answer<?php echo $QuestionNo; ?>" id="<?php echo $Label; ?>"
                    value="<?php echo $Letter; ?>" />
                <label for="<?php echo $Label; ?>"><?php echo $Letter; ?>) <?php echo $Answer; ?> </label>
            </div>
            <?php }

                    if ($QuestionNo == $totalQuestionCount) {
                        break;
                    }
                    ?>
            <?php } ?>
            <div class="btn-block">
                <button type="submit">Next Page</button>
                <!-- <input type='submit' class="button" name="next" value='Next Page' /> -->
            </div>
        </form>
    </div>

</body>

</html>