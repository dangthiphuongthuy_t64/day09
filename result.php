<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
</head>


<body>
    <?php
    $Questions = array(
        1 => array(
            'Question' => '1. CSS stands for',
            'Answers' => array(
                'A' => 'Computer Styled Sections',
                'B' => 'Cascading Style Sheets',
                'C' => 'Crazy Solid Shapes'
            ),
            'CorrectAnswer' => 'B'
        ),
        2 => array(
            'Question' => '2. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        3 => array(
            'Question' => '3. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        4 => array(
            'Question' => '4. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        5 => array(
            'Question' => '5. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        6 => array(
            'Question' => '6. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        7 => array(
            'Question' => '7. CSS stands for',
            'Answers' => array(
                'A' => 'Computer Styled Sections',
                'B' => 'Cascading Style Sheets',
                'C' => 'Crazy Solid Shapes'
            ),
            'CorrectAnswer' => 'B'
        ),
        8 => array(
            'Question' => '8. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        9 => array(
            'Question' => '9. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        10 => array(
            'Question' => '10. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        )
    );
    ?>
    <?php
        $listResult = array("B", "C", "C", "C", "C", "C", "B", "C", "C", "C");
        $score = 0;
        for ($i = 1; $i <= 10; $i++) {
            $key = "answer" . strval($i);
            if ($_COOKIE[$key] == $listResult[$i - 1]) {
                $score++;
            } else {
            echo 'You answered: <span style="color: red;">' . $i .".". $_COOKIE[$key] . '</span><br>'; // Replace style with a class
            echo 'Correct answer: <span style="color: green;">' . $i . "." . $listResult[$i - 1] . '</span><br>';
            }
        }

    ?>
    <div>
        <h3 class="title">Kết quả</h3> 
        <h4 class="score">Điểm của bạn: <?php echo $score; ?>/10.</h4>
        <h4 class="description">
            =>
            <?php
            if ($score < 4) {
                echo 'Bạn quá kém, cần ôn tập thêm!';
            } elseif ($score <= 7) {
                echo 'Cũng bình thường!';
            } else {
                echo 'Sắp sửa làm được trợ giảng lớp PHP!';
            }
            ?>
        </h4>
        <div class="btn-block">
            <button type="submit" href="/">Back</button>
        </div>
    </div>
</body>

</html>