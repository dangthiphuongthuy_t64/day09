<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz</title>
</head>

<body>
    <?php
    $Questions = array(
        6 => array(
            'Question' => '6. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        7 => array(
            'Question' => '7. CSS stands for',
            'Answers' => array(
                'A' => 'Computer Styled Sections',
                'B' => 'Cascading Style Sheets',
                'C' => 'Crazy Solid Shapes'
            ),
            'CorrectAnswer' => 'B'
        ),
        8 => array(
            'Question' => '8. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        9 => array(
            'Question' => '9. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        ),
        10 => array(
            'Question' => '10. What is the Capital of the Philippines',
            'Answers' => array(
                'A' => 'Cebu City',
                'B' => 'Davao City',
                'C' => 'Manila City'
            ),
            'CorrectAnswer' => 'C'
        )
    );

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST["answer6"]) && isset($_POST["answer7"]) && isset($_POST["answer8"]) && isset($_POST["answer9"]) && isset($_POST["answer10"])) {
            $answer6 = $_POST["answer6"];
            $answer7 = $_POST["answer7"];
            $answer8 = $_POST["answer8"];
            $answer9 = $_POST["answer9"];
            $answer10 = $_POST["answer10"];
            setcookie("answer6", $answer6, time() + (86400 * 30), "/");
            setcookie("answer7", $answer7, time() + (86400 * 30), "/");
            setcookie("answer8", $answer8, time() + (86400 * 30), "/");
            setcookie("answer9", $answer9, time() + (86400 * 30), "/");
            setcookie("answer10", $answer10, time() + (86400 * 30), "/");
            header("location: result.php");
        } else {
            echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
        }
    }
    ?>
        <div class="testbox">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="quiz">
                <div class="banner">
                </div>
                <?php
                $totalQuestionCount = count($Questions);
                foreach ($Questions as $QuestionNo => $Value) {

                ?>

                    <h3><?php echo $Value['Question']; ?></h3>
                    <?php
                    foreach ($Value['Answers'] as $Letter => $Answer) {
                        $Label = 'question-' . $QuestionNo . '-answers-' . $Letter;
                    ?>
                        <div>
                            <input type="radio" name="answer<?php echo $QuestionNo; ?>" id="<?php echo $Label; ?>" value="<?php echo $Letter; ?>" />
                            <label for="<?php echo $Label; ?>"><?php echo $Letter; ?>) <?php echo $Answer; ?> </label>
                        </div>
                    <?php }

                    if ($QuestionNo == $totalQuestionCount) {
                        break;
                    }
                    ?>
                <?php } ?>
                <div class="btn-block">
                    <button type="submit" href="/">Submit</button>
                    <!-- <input type='submit' class="button" name="next" value='Submit' /> -->
                </div>
            </form>
        </div>

</body>

</html>